package com.example.demo.util;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;

import com.example.demo.model.Status;
import com.example.demo.model.TicketEntity;
import com.example.demo.model.TicketModel;


public class TicketBuilder {
	
	private TicketModel ticket;
	  
	public TicketBuilder() {
		this.ticket = new TicketModel();
	}
	
	public TicketBuilder(TicketEntity ticket) {
		this.ticket = new TicketModel();
		
		setName(ticket.getName()).setDetail(ticket.getDetail()).setContact(ticket.getContact())
		.setCreatedDate(ticket.getCreatedDate()).setUpdateDate(ticket.getUpdateDate()).setStatus(ticket.getStatus());
	}
	
	public static TicketBuilder getTicketBuilder() {
		return new TicketBuilder();
	}
	
	public static TicketBuilder getTicketBuilder(TicketEntity tciket) {
		return new TicketBuilder(tciket);
	}
	
	/**
	 * 
	 */
	public static TicketModel getTicketModel(TicketEntity ticket) {
		if(ticket == null) {
			return null;
		}
		
		return new TicketBuilder(ticket).build();
	}
	
	public static List<TicketModel> getTicketModel(Collection<TicketEntity> ticket) {
		if(ticket == null) {
			return null;
		}
		
		List<TicketModel> dtos = new ArrayList<>();
		ticket.forEach(t -> dtos.add(TicketBuilder.getTicketBuilder(t).build()));
		
		return dtos;
	}
	
	/**
	 * 
	 */
	public TicketBuilder setName(String name) {
		this.ticket.setName(name);
		return this;
	}
	
	public TicketBuilder setDetail(String detail) {
		this.ticket.setDetail(detail);
		return this;
	}
	
	public TicketBuilder setContact(String contact) {
		this.ticket.setContact(contact);
		return this;
	}
	
	public TicketBuilder setCreatedDate(Date createdDate) {
		this.ticket.setCreatedDate(createdDate);
		return this;
	}
	
	public TicketBuilder setUpdateDate(Date updateDate) {
		this.ticket.setUpdateDate(updateDate);
		return this;
	}
	
	public TicketBuilder setStatus(Status status) {
		this.ticket.setStatus(status);
		return this;
	}
	
	public TicketModel build() {
		return this.ticket;
	}

}
