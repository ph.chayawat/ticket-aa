package com.example.demo.service;

import java.util.Date;
import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Collectors;


import com.example.demo.controller.TicketController;
import com.example.demo.model.Status;
import com.example.demo.model.TicketBody;
import com.example.demo.model.TicketEntity;
import com.example.demo.model.TicketModel;
import com.example.demo.util.TicketBuilder;

public class TicketService {

	public static TicketModel findTicketByName(String name) {

		TicketModel ticketModel = new TicketModel();

		List<TicketEntity> entitys = TicketController.TicketEntity();


		TicketEntity entity = findTicketEntityByName(name, entitys);

		ticketModel = TicketBuilder.getTicketModel(entity);

		return ticketModel;
	}

	public static List<TicketModel> getTicketAll() {

		List<TicketEntity> entitys = TicketController.TicketEntity();

		return TicketBuilder.getTicketModel(entitys);
	}

	public static TicketModel saveTicket(TicketBody body) {

		TicketModel ticketModel = new TicketModel();

		TicketEntity entity = new TicketEntity();
		
		entity.setName(body.getName());
		entity.setDetail(body.getDetail());
		entity.setContact(body.getContact());
		entity.setCreatedDate(new Date());
		entity.setStatus(body.getStatus());

		ticketModel = TicketBuilder.getTicketModel(entity);

		return ticketModel;
	}

	public static TicketModel updateTicket(String name, TicketBody body) {

		TicketModel ticketModel = new TicketModel();

		List<TicketEntity> entitys = TicketController.TicketEntity();

		TicketEntity entity = findTicketEntityByName(name, entitys);
		
		entity.setName(body.getName());
		entity.setDetail(body.getDetail());
		entity.setContact(body.getContact());
		entity.setUpdateDate(new Date());
		entity.setStatus(body.getStatus());

		ticketModel = TicketBuilder.getTicketModel(entity);

		return ticketModel;
	}

	public static TicketModel updateTicketStatus(String name, Status status) {

		TicketModel ticketModel = new TicketModel();

		List<TicketEntity> entitys = TicketController.TicketEntity();

		TicketEntity entity = findTicketEntityByName(name, entitys);
		
		entity.setUpdateDate(new Date());
		entity.setStatus(status);

		ticketModel = TicketBuilder.getTicketModel(entity);

		return ticketModel;
	}
	
	private static TicketEntity findTicketEntityByName(String name, List<TicketEntity> entitys) {
		TicketEntity  entity = new TicketEntity();
        
        List<TicketEntity> ae = null;
        
        Predicate<TicketEntity> predicate = a->  a.getName().equals(name);     
        ae = entitys.stream().filter(predicate).collect(Collectors.toList());

        entity = ae.get(0);
        
        return entity;
	}

}
