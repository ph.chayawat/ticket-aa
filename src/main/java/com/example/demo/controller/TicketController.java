package com.example.demo.controller;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.model.Status;
import com.example.demo.model.TicketBody;
import com.example.demo.model.TicketEntity;
import com.example.demo.model.TicketModel;
import com.example.demo.service.TicketService;


@RestController
@RequestMapping("/api/v1/products")
public class TicketController {
	
	 public static List<TicketEntity> TicketEntity() {
		 
		 List<TicketEntity> entitys = new ArrayList<>(Arrays.asList(
		            new TicketEntity("name 1", "detail 1", "contact 1", Status.PENDING, new Date(), null),
		            new TicketEntity("name 2", "detail 2", "contact 2", Status.ACCEPTED, new Date(), null),
		            new TicketEntity("name 3", "detail 3", "contact 3", Status.ACCEPTED, new Date(), null),
		            new TicketEntity("name 4", "detail 4", "contact 4", Status.ACCEPTED, new Date(), null)
					));
		 
		 return entitys;
		
	}
	
	@GetMapping("/{name}")
	public ResponseEntity<TicketModel> findTicketByName(@PathVariable String name) {
		
		// TODO get product by id from DB 
		
		return ResponseEntity.ok().body(TicketService.findTicketByName(name));
	}
	
	@PostMapping("/all")
	public ResponseEntity<List<TicketModel>> getTicketAll() {
		
		// TODO save product to DB
		
		return ResponseEntity.ok().body(TicketService.getTicketAll());
	}
	
	@PostMapping("/save")
	public ResponseEntity<TicketModel> saveTicket(@RequestBody TicketBody body) {
		
		// TODO save product to DB
		
		return ResponseEntity.ok().body(TicketService.saveTicket(body));
	}
	
	@PutMapping("/update/{name}")
	public ResponseEntity<TicketModel> updateTicket(@PathVariable String name, @RequestBody TicketBody body) {
		
		// TODO update product to DB
		
		return ResponseEntity.ok().body(TicketService.updateTicket(name, body));
	}
	
	@DeleteMapping("/update/status/{name}")
	public ResponseEntity<TicketModel> updateTicketStatus(@PathVariable String name, @RequestParam Status status) {
		
		// TODO delete product by id from DB
		
		return ResponseEntity.ok().body(TicketService.updateTicketStatus(name, status)); 
	}

}
