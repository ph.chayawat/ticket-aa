package com.example.demo.model;

import java.io.Serializable;
import java.util.Date;

public class TicketEntity implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -1643306195478675696L;
	private String name = null;
	private String detail = null;
	private String contact = null;
	private Date createdDate = new Date();
	private Date updateDate = null;
	private Status status = Status.PENDING;
	
	public TicketEntity() {
	}
	
	public TicketEntity(String name, String detail, String contact, Status status, Date createdDate, Date updateDate ) {
		super();
		this.name = name;
		this.detail = detail;
		this.contact = contact;
		this.status = status;
		this.createdDate = createdDate;
		this.updateDate = updateDate;
	}
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getDetail() {
		return detail;
	}
	public void setDetail(String detail) {
		this.detail = detail;
	}
	public String getContact() {
		return contact;
	}
	public void setContact(String contact) {
		this.contact = contact;
	}
	public Date getCreatedDate() {
		return createdDate;
	}
	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}
	public Date getUpdateDate() {
		return updateDate;
	}
	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}
	public Status getStatus() {
		return status;
	}
	public void setStatus(Status status) {
		this.status = status;
	}
	@Override
	public String toString() {
		return "TicketModel [name=" + name + ", detail=" + detail + ", contact=" + contact + ", createdDate="
				+ createdDate + ", updateDate=" + updateDate + ", status=" + status + "]";
	}
}
