package com.example.demo.model;

import com.fasterxml.jackson.annotation.JsonValue;

public enum Status {
	
	PENDING("PENDING"), 
	ACCEPTED("ACCEPTED"),
	RESOLVED("RESOLVED"),
	REJECTED("REJECTED");
	
	private final String value;
	
	private Status(String value) {
		this.value = value;
	}

	@Override
	@JsonValue
	public String toString() {
		return String.valueOf(value);
	}


}
