package com.example.demo.model;

import java.io.Serializable;
import java.util.Date;

public class TicketBody implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 7071082518766044161L;

	private String name = null;
	private String detail = null;
	private String contact = null;
	private Status status = Status.PENDING;
	
	public TicketBody(String name,	String detail, String contact, Status status) {
		super();
		this.name = name;
		this.detail = detail;
		this.contact = contact;
		this.status = status;
	}
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getDetail() {
		return detail;
	}
	public void setDetail(String detail) {
		this.detail = detail;
	}
	public String getContact() {
		return contact;
	}
	public void setContact(String contact) {
		this.contact = contact;
	}
	public Status getStatus() {
		return status;
	}
	public void setStatus(Status status) {
		this.status = status;
	}
	@Override
	public String toString() {
		return "TicketBody [name=" + name + ", detail=" + detail + ", contact=" + contact + ", status=" + status + "]";
	}
	
	
}
